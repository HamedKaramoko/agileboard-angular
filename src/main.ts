import { enableProdMode, TRANSLATIONS, TRANSLATIONS_FORMAT, MissingTranslationStrategy } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

// use the require method provided by webpack
//declare const require;
// we use the webpack raw-loader to return the content as a string
//const translations = require(`raw-loader!./locale/messages.en.xlf`);

platformBrowserDynamic().bootstrapModule(AppModule, {
  missingTranslation: MissingTranslationStrategy.Error,
  providers: [
    /* ng serve --aot --locale fr
    xliffmerge
    https://medium.com/@feloy/deploying-an-i18n-angular-app-with-angular-cli-fc788f17e358
    */
    /*{provide: TRANSLATIONS, useValue: translations},
    {provide: TRANSLATIONS_FORMAT, useValue: 'xlf'}*/
  ]
}).catch(err => console.log(err));
