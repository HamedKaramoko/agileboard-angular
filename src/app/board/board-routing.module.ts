import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardComponent } from './board.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'board',
    component: BoardComponent,
    children: []
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class BoardRoutingModule { }
