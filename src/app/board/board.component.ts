import { Component, OnInit } from '@angular/core';
import { UserStoryService } from '../core/services/user-story.service';

@Component({
		selector: 'app-board',
		templateUrl: './board.component.html',
		styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

		constructor(private userStoryService: UserStoryService) { }

		ngOnInit() {
			this.userStoryService.getUserStories().subscribe((data) => {
				console.log(JSON.stringify(data));
			});
		}

}
