import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Observable } from 'rxjs';
import { UserStory } from '../models/user-story';
import * as faker from 'faker/locale/fr';

export class InMemoryUserStoryService implements InMemoryDbService {

	createDb(): {} {
		const userStories: UserStory[] = manyUserStories(10);
		return {userStories};
	}
}

const aUserStory = (): UserStory => {
	const uS: UserStory = {
		id: faker.random.number(15),
		label: faker.random.word(),
		description: faker.random.word(),
		priority: faker.random.number(100)
	};
	return uS;
};

const manyUserStories = (count = faker.random.number(20)): UserStory[] => {
	const response = [];
	for (let i = 0; i < 10; i++) {
		response.push(aUserStory());
	}
	return response;
};
