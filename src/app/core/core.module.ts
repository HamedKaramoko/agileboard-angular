import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { CoreRoutingModule } from './core-routing.module';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RouterModule } from '@angular/router';
import { StoryBoardService } from './services/story-board.service';
import { InMemoryUserStoryService } from './mocks/in-memory-user-story.service';
import { environment } from '../../environments/environment';
import { BoardModule } from '../board/board.module';
import { AdminModule } from '../admin/admin.module';
import { UserStoryService } from './services/user-story.service';

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		environment.production ? [] : HttpClientInMemoryWebApiModule.forRoot(InMemoryUserStoryService),
		BoardModule,
		AdminModule,
		CoreRoutingModule
	],
	exports: [
		RouterModule,
		HeaderComponent
	],
	declarations: [HeaderComponent, LoginComponent, NotFoundComponent],
	providers: [StoryBoardService, UserStoryService]
})
export class CoreModule { }
