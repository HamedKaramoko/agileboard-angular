export class Actor {
    id: number;
    name: string;
    role: string;
}
