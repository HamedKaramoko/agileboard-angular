export class UserStory {
    id: number;
    label: string;
    description: string;
    priority: number;
}
