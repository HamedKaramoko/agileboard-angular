import { Step } from './step';

export class Activity {
    id: number;
    label: string;
    description: string;
    previousActivity: Activity;
    nextActivity: Activity;
    steps: Step[];
}
