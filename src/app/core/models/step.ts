import { Actor } from './actor';
import { UserStory } from './user-story';

export class Step {
    id: number;
    label: string;
    description: string;
    previousStep: Step;
    nextStep: Step;
    actors: Actor[];
    userStories: UserStory[];
}
