import { Activity } from './activity';

export class StoryBoard {
    id: number;
    label: string;
    description: string;
    activity: Activity[];
}
