import { TestBed, inject } from '@angular/core/testing';

import { StoryBoardService } from './story-board.service';

describe('StoryBoardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StoryBoardService]
    });
  });

  it('should be created', inject([StoryBoardService], (service: StoryBoardService) => {
    expect(service).toBeTruthy();
  }));
});
