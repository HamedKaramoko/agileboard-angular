import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { UserStory } from '../models/user-story';
import { Observable } from 'rxjs';
import { AppConfigService } from './app-config.service';

@Injectable({
	providedIn: 'root'
})
export class UserStoryService {

	constructor(private httpClient: HttpClient, private appConfigService: AppConfigService) { }

	getUserStories(): Observable<UserStory[]> {
		return this.httpClient.get<UserStory[]>(`${this.appConfigService.apiUrl}/userStories`);
	}
}
